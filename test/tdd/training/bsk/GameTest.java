package tdd.training.bsk;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class GameTest {
	
	private Game game;
	
	@Before
	public void setUp() {
		game = new Game();	
	}
	
	@Test
	// user story #3
	public void testGetFrameAt() throws BowlingException {
		Frame frame1 = new Frame(2, 5);
		Frame frame2 = new Frame(3, 7);
		game.addFrame(frame1);
		game.addFrame(frame2);
		
		assertEquals(frame2, game.getFrameAt(1));
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	// user story #3
	public void testGetFrameAtInvalidIndex() throws BowlingException {
		game.getFrameAt(10);
	}

	@Test(expected = BowlingException.class)
	// user story #3
	public void testAddFrame() throws BowlingException {
		for (int i = 0; i < 12; i++)
			game.addFrame(new Frame(0, 0));
	}
	
	@Test
	// user story #4
	public void testCalculateScoreWithNoBonus() throws BowlingException {
		int expected = 4*10;
		for (int i = 0; i < 10; i++) 
			game.addFrame(new Frame(2, 2));
		assertEquals(expected, game.calculateScore());
	}
	
	@Test
	// user story #5
	public void testCalculateScoreWithSpareBonus() throws BowlingException {
		int expected = 100+45;
		for (int i = 0; i < 10; i++) 
			game.addFrame(new Frame(i, 10-i));
		assertEquals(expected, game.calculateScore());
	}
	
	@Test
	// user story #6
	public void testCalculateScoreWithStrikeBonus() throws BowlingException {
		int expected = 10+10+10;
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		assertEquals(expected, game.calculateScore());
	}
	
	@Test
	// user story #7
	public void testCalculateScoreWithConsecutiveStrikeAndSpare() throws BowlingException {
		int expected = 29+10+7;
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame( 4, 6));
		game.addFrame(new Frame( 7, 2));
		assertEquals(expected, game.calculateScore());
	}
	
	@Test
	// user story #8
	public void testCalculateScoreWithMultipleStrikes() throws BowlingException {
		int expected = 29+10+7+9;
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame( 7, 2));
		assertEquals(expected, game.calculateScore());
	}
	
	@Test
	// user story #9
	public void testCalculateScoreWithMultipleSpares() throws BowlingException {
		int expected = 15+17+9;
		game.addFrame(new Frame(8, 2));
		game.addFrame(new Frame(5, 5));
		game.addFrame(new Frame(7, 2));
		assertEquals(expected, game.calculateScore());
	}
	
	@Test
	// user story #10
	public void testGetFirstBonusThrow() throws BowlingException {
		game.setFirstBonusThrow(7);
		assertEquals(7, game.getFirstBonusThrow());
	}
	
	@Test
	// user story #10
	public void testCalculateScoreWithSpareAsLastFrame() throws BowlingException {
		game.setFirstBonusThrow(7);
		
		int expected = 4*9+10+7;
		for(int i = 0; i < 9; i++) {
			game.addFrame(new Frame(2,2));
		}
		game.addFrame(new Frame(8, 2));
		
		assertEquals(expected, game.calculateScore());
	}
	
	@Test
	// user story #11
	public void testGetSecondBonusThrow() throws BowlingException {
		game.setSecondBonusThrow(2);
		assertEquals(2, game.getSecondBonusThrow());
	}
	
	@Test
	// user story #11
	public void testCalculateScoreWithStrikeAsLastFrame() throws BowlingException {
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		int expected = 4*9+10+7+2;
		for(int i = 0; i < 9; i++) {
			game.addFrame(new Frame(2, 2));
		}
		game.addFrame(new Frame(10, 0));
		
		assertEquals(expected, game.calculateScore());
	}
	
	@Test
	// user story #12
	public void testCalculateScoreOfPerfectGame() throws BowlingException {
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		int expected = 300;
		for(int i = 0; i < 10; i++) {
			game.addFrame(new Frame(10, 0));
		}
		
		assertEquals(expected, game.calculateScore());
	}
}
