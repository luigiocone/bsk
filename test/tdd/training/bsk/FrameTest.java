package tdd.training.bsk;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


public class FrameTest {

	@Test
	// user story #1
	public void testGetFirstThrow() throws BowlingException {
		Frame frame = new Frame(2, 5);
		assertEquals(2, frame.getFirstThrow());
	}
	
	@Test
	// user story #1
	public void testGetSecondThrow() throws BowlingException {
		Frame frame = new Frame(2, 5);
		assertEquals(5, frame.getSecondThrow());
	}
	
	@Test(expected = BowlingException.class)
	// user story #1
	public void testWrongFrameConstruction() throws BowlingException {
		new Frame(8, 9);
	}
	
	@Test
	// user story #2
	public void testGetScore() throws BowlingException {
		Frame frame = new Frame(2, 5);
		assertEquals(7, frame.getScore());
	}
	
	@Test
	// user story #5
	public void testGetBonus() throws BowlingException {
		Frame frame = new Frame(5, 5);
		frame.setBonus(3);
		assertEquals(3, frame.getBonus());
	}
	
	@Test
	// user story #5
	public void testIsSpare() throws BowlingException {
		assertTrue(new Frame(5, 5).isSpare());
	}
	
	@Test
	// user story #5
	public void testIsNotSpare() throws BowlingException {
		assertTrue(!(new Frame(4, 5).isSpare()));
	}
	
	@Test
	// user story #5
	public void testGetScoreWithBonus() throws BowlingException {
		Frame frame = new Frame(5, 5);
		frame.setBonus(2);
		assertEquals(12, frame.getScore());
	}
	
	@Test
	// user story #6
	public void testIsStrike() throws BowlingException {
		assertTrue(new Frame(10, 0).isStrike());
	}
	
	@Test
	// user story #6
	public void testIsNotStrike() throws BowlingException {
		assertTrue(!(new Frame(9, 0).isStrike()));
	}
	
	@Test
	// user story #6
	public void testIsStrikeButNoSpare() throws BowlingException {
		Frame frame = new Frame(10, 0);
		assertTrue(frame.isStrike());
		assertTrue(!frame.isSpare());
	}

}
