package tdd.training.bsk;

import java.util.ArrayList;
import java.util.List;

public class Game {

	private List<Frame> frames;
	private int firstBonusThrow;
	private int secondBonusThrow;
	
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frames = new ArrayList<>();
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if(frames.size() == 10)
			throw new BowlingException();
		frames.add(frame);
		// size now is incremented by 1
		
		int size = frames.size();
		if (size < 2) 
			return;
		
		
		if(size == 10) this.assignBonusAtFrames(size-1, true);
		this.assignBonusAtFrames(size-1, false);
	}
	
	/**
	 * Assign bonuses to previous or last frames
	 * @param last
	 */
	public void assignBonusAtFrames(int last, boolean end) {
		Frame currFrame = frames.get(last);
		Frame prevFrame = frames.get(last-1);
		
		int firstThrow, secondThrow;
		if (end) {
			firstThrow = this.firstBonusThrow;
			secondThrow = this.secondBonusThrow;
			prevFrame = currFrame;
		}
		else {
			firstThrow = currFrame.getFirstThrow();
			secondThrow = currFrame.getSecondThrow();
		}
		
		
		if(prevFrame.isSpare()) 
			prevFrame.setBonus(firstThrow);
		else if(prevFrame.isStrike()) {		
			prevFrame.setBonus(prevFrame.getBonus() + firstThrow + secondThrow);
			if(last-1 > 0 && (prevFrame = frames.get(last-2)).isStrike()) {
				prevFrame.setBonus(prevFrame.getBonus() + firstThrow);
			}
		}
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return frames.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		// To be implemented
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		// To be implemented
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		for (int i = 0; i < frames.size(); i++) 
			score += frames.get(i).getScore();
		return score;
	}

}
